// ----------------------------------------------------------------------

const account = {
  displayName: 'Muhammad Ismail',
  email: 'mi477048@gmail.com',
  photoURL: '/static/mock-images/avatars/avatar_default.jpg'
};

export default account;

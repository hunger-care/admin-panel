/* eslint-disable */
import { React, useEffect, useState } from 'react';
import Page from '../components/Page';
import OrderDetail from './OrderDetail';
import User from './User';
import { getOrderAboutUser, getOrderAboutDhobie } from '../services/api';

export default function OrderPage({ setIsOrder, orderId, name, isAdmin }) {
  const [totalFeedback, setTotalFeedback] = useState([]);
  useEffect(() => {
    async function fetchData() {
      try {
        if (isAdmin) {
          const response = await getOrderAboutDhobie(orderId);
          setTotalFeedback(response.data);
        } else {
          const response = await getOrderAboutUser(orderId);
          setTotalFeedback(response.data);
        }
        console.log(response);
      } catch (e) {
        console.error(e);
      }
    }
    fetchData();
  }, []);

  // console.log(totalFeedback);
  return (
    <Page title="Dashboard: Dhobies | Admin-panel">
      <OrderDetail
        setIsOrder={setIsOrder}
        totalFeedback={totalFeedback}
        title="Customers feedback"
        name={name}
      />
    </Page>
  );
}

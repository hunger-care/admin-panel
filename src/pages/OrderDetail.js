/* eslint-disable */
import React, { useState } from 'react';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { Typography } from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import DetailOfComment from './DetailOfComment';

function createData(id, name, email, userType, comments) {
  return { id, name, email, userType, comments };
}

function OrderDetail({ title, totalFeedback, setIsOrder, name }) {
  const [isMessage, setIsMessage] = useState();
  const handleBack = () => {
    setIsOrder(false);
  };
  // console.log(totalFeedback);
  // const handleMessage = () => {
  //   setTimeout(() => {
  //     if (totalFeedback.length === 0) {
  //       // console.log('This will run after 1 second!');
  //       setIsMessage('There is no order  placed to this user');
  //     }
  //   }, 2000);
  // };

  return (
    <div>
      <Typography mb={5} ml={3}>
        <Typography variant="h4" gutterBottom>
          <ArrowBackIcon
            onClick={handleBack}
            style={{ marginBottom: '-3px', marginRight: '5px' }}
          />
          Placed Order to {name}
        </Typography>
      </Typography>

      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} size="medium" aria-label="a dense table">
          <TableHead style={{ fontSize: '15px' }}>
            <TableRow>
              <TableCell align="center">Orde Type</TableCell>
              <TableCell align="center">Order Price</TableCell>
              <TableCell align="center">Order Address</TableCell>
              <TableCell align="center">Pickup Date</TableCell>
              <TableCell align="center">Pickup Time</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <Typography style={{}}>
              {/* {totalFeedback.length === 0 ? 'There is no order  placed to this user' : <></>} */}
              {/* {isMessage}
              {handleMessage()} */}
            </Typography>{' '}
            {totalFeedback.map((row) => (
              <TableRow
                style={{ cursor: 'pointer' }}
                hover="true"
                key={row.name}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableCell align="center">{row.order_type}</TableCell>
                <TableCell align="center">{row.order_price}</TableCell>
                <TableCell align="center">{row.order_address}</TableCell>
                <TableCell align="center">{row.order_pickDate}</TableCell>
                <TableCell align="center">{row.order_pickTime}</TableCell>

                <TableCell style={{ paddingLeft: '50px' }} align="left">
                  {/* {truncate(row.message)} */}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}

export default OrderDetail;

/* eslint-disable */
import { filter } from 'lodash';
import { Icon } from '@iconify/react';
import { sentenceCase } from 'change-case';
import { useState, useEffect } from 'react';
import plusFill from '@iconify/icons-eva/plus-fill';
import { Link as RouterLink } from 'react-router-dom';
// material
import {
  Card,
  Table,
  Stack,
  Avatar,
  Button,
  Checkbox,
  TableRow,
  TableBody,
  TableCell,
  Container,
  Typography,
  TableContainer,
  TablePagination
} from '@mui/material';
// components
import Page from '../components/Page';
import Label from '../components/Label';
import Scrollbar from '../components/Scrollbar';
import SearchNotFound from '../components/SearchNotFound';
import { UserListHead, UserListToolbar, UseMoreUser } from '../components/_dashboard/user';
//
// import totalRegUser from '../_mocks_/Useruser';
import { getAllUsers } from '../services/api';
import EditForm from './EditForm';
import OrderPage from '../pages/OrderPage';
// ----------------------------------------------------------------------

const TABLE_HEAD = [
  { id: 'name', label: 'Name', alignRight: false },
  { id: 'role', label: 'Email', alignRight: false },
  { id: 'isVerified', label: 'Mobile Number', alignRight: false },
  // { id: 'status', label: 'Status', alignRight: false },
  { id: '' }
];

// ----------------------------------------------------------------------

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function applySortFilter(array, comparator, query) {
  const stabilizedThis = array?.map((el, index) => [el, index]);
  stabilizedThis?.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  if (query) {
    return filter(
      array,
      (_user) => _user.username?.toLowerCase().indexOf(query?.toLowerCase()) !== -1
    );
  }
  return stabilizedThis?.map((el) => el[0]);
}

export default function User({ title }) {
  const [isEdit, setIsEdit] = useState(false);
  const [editId, setEditId] = useState({});
  const [totalRegUser, setTotalRegUser] = useState([]);
  const [selectUser, setSelectUser] = useState([]);
  // console.log(isEdit);
  // console.log(editId);
  useEffect(() => {
    async function fetchData() {
      try {
        const response = await getAllUsers();
        setTotalRegUser(response.data);
      } catch (e) {
        console.error(e);
      }
    }
    fetchData();
  });
  // console.log(totalRegUser);
  const [page, setPage] = useState(0);
  const [order, setOrder] = useState('asc');
  const [selected, setSelected] = useState([]);
  const [orderBy, setOrderBy] = useState('username');
  const [filterName, setFilterName] = useState('');
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [isExist, setIsExist] = useState(false);
  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = totalRegUser.map((n) => n.username);
      setSelectUser(totalRegUser.map((n) => n._id));
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };
  const userClick = (id) => {
    const newData = selectUser.filter((e) => e !== id);
    console.log(newData);

    // setSelectUser(newData);
    // console.log(isExist);

    // const newData = selectUser.filter((e) => {
    //   if (e != id) {
    //     setIsExist(true);
    //     return e != id;
    //   }
    // });

    // selectUser.map((e) => {
    //   if (e == id) {

    //   }
    // });
  };

  const handleClick = (event, username, _id) => {
    if (selectUser.includes(_id)) {
      setSelectUser(selectUser.filter((e) => e !== _id));
    } else {
      setSelectUser([...selectUser, _id]);
    }

    //code to practice above login on different method

    // console.log('force called');
    // const newData = selectUser.filter((e) => e !== _id);
    // setSelectUser(newData);
    // console.log(selectUser);
    // setSelectUser([...selectUser, _id]);
    // console.log(selectUser);

    const selectedIndex = selected.indexOf(username);
    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, username);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    setSelected(newSelected);
  };
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFilterByName = (event) => {
    setFilterName(event.target.value);
  };

  const emptyRows = page > 0 ? Math.max(0, (1 + page) * rowsPerPage - totalRegUser?.length) : 0;

  const filteredUsers = applySortFilter(totalRegUser, getComparator(order, orderBy), filterName);

  const isUserNotFound = filteredUsers?.length === 0;

  // Test the selected user through the following line
  // console.log(selectUser);
  const [isOrder, setIsOrder] = useState(false);
  const [orderId, setOrderId] = useState('');
  const [name, setName] = useState('');
  const [isAdmin, setIsAdmin] = useState(false);
  const handleOrderClick = (id, username) => {
    setIsOrder(true);
    setOrderId(id);
    setName(username);
    // console.log(id);
  };

  return (
    <Page title="User | Admin-panel">
      {isOrder ? (
        <OrderPage isAdmin={isAdmin} name={name} orderId={orderId} setIsOrder={setIsOrder} />
      ) : (
        <>
          <Container>
            <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
              {title ? (
                <Typography variant="h4" gutterBottom>
                  {title}
                </Typography>
              ) : (
                <Typography variant="h4" gutterBottom>
                  {isEdit ? '' : 'Register Users list'}
                </Typography>
              )}
              {/* <Typography variant="h4" gutterBottom>
            All Register User
          </Typography> */}
            </Stack>

            <Card>
              {!isEdit ? (
                <>
                  <UserListToolbar
                    numSelected={selected.length}
                    filterName={filterName}
                    onFilterName={handleFilterByName}
                    selectUser={selectUser}
                    setSelected={setSelected}
                  />

                  <Scrollbar>
                    <TableContainer sx={{ minWidth: 800 }}>
                      <Table>
                        <UserListHead
                          order={order}
                          orderBy={orderBy}
                          headLabel={TABLE_HEAD}
                          rowCount={totalRegUser?.length}
                          numSelected={selected.length}
                          onRequestSort={handleRequestSort}
                          onSelectAllClick={handleSelectAllClick}
                        />

                        <TableBody>
                          {totalRegUser?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                            .map((row) => {
                              const {
                                _id,
                                username,
                                email,
                                id,
                                name,
                                mobile_no,
                                role,
                                Address,
                                avatarUrl,
                                isVerified,
                                profilePic
                              } = row;
                              const isItemSelected = selected.indexOf(username) !== -1;

                              return (
                                <TableRow
                                  hover
                                  key={_id}
                                  tabIndex={-1}
                                  role="checkbox"
                                  selected={isItemSelected}
                                  aria-checked={isItemSelected}
                                  // onClick={() => handleOrderClick(_id, username)}
                                >
                                  <TableCell padding="checkbox">
                                    <Checkbox
                                      checked={isItemSelected}
                                      onChange={(event) => handleClick(event, username, _id)}
                                    />
                                  </TableCell>
                                  <TableCell
                                    onClick={() => handleOrderClick(_id, username)}
                                    component="th"
                                    scope="row"
                                    padding="none"
                                  >
                                    <Stack direction="row" alignItems="center" spacing={2}>
                                      <Avatar alt={username} src={profilePic} />
                                      <Typography variant="subtitle2" noWrap>
                                        {username}
                                      </Typography>
                                    </Stack>
                                  </TableCell>
                                  <TableCell
                                    onClick={() => handleOrderClick(_id, username)}
                                    align="left"
                                  >
                                    {email}
                                  </TableCell>
                                  <TableCell
                                    onClick={() => handleOrderClick(_id, username)}
                                    align="left"
                                  >
                                    {mobile_no}
                                  </TableCell>

                                  <TableCell align="left">
                                    <UseMoreUser
                                      row={row}
                                      setEditId={setEditId}
                                      setIsEdit={setIsEdit}
                                      _id={_id}
                                    />
                                  </TableCell>
                                </TableRow>
                              );
                            })}
                          {emptyRows > 0 && (
                            <TableRow style={{ height: 53 * emptyRows }}>
                              <TableCell colSpan={6} />
                            </TableRow>
                          )}
                        </TableBody>
                        {isUserNotFound && (
                          <TableBody>
                            {/* <TableRow>
                      <TableCell align="center" colSpan={6} sx={{ py: 3 }}>
                        <SearchNotFound searchQuery={filterName} />
                      </TableCell>
                    </TableRow> */}
                          </TableBody>
                        )}
                      </Table>
                    </TableContainer>
                  </Scrollbar>

                  <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={totalRegUser?.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                  />
                </>
              ) : (
                <div>
                  <EditForm setIsEdit={setIsEdit} editId={editId} />
                </div>
              )}
            </Card>
          </Container>
        </>
      )}
    </Page>
  );
}
